import dependu as dp

def test_strip_accents():
    assert dp.strip_accents("azertŷùï") == "azertyui"
    assert dp.strip_accents("") == ""
    assert dp.strip_accents("ç") == "c"
    assert dp.strip_accents("1234./?&") == "1234./?&"
    assert dp.strip_accents("azertÿ") == "azerty"

def test_dict_reader():
    assert dp.dict_reader()

def test_create_dicts():
    pass

def test_load_dicts():
    pass

def test_is_valid():
    pass

def test_load():
    pass

def test_get_possible_word_list():
    pass

def test_get_result():
    pass

def test_main():
    pass
