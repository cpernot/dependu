import os
from setuptools import setup

setup(
    name="dependu",
    version="0.1.0",
    author="calf",
    author_email="",
    description=(""),
    license="GPLv3+",
    url="http://",
    packages=['dependu'],
    long_description=open(os.path.join(os.path.dirname(__file__),
                                       'README.md')).read(),

    python_requires=">=3.6",
    setup_requires=[],
    tests_require=['pytest', ],
    install_requires=[],
    package_data={},
    classifiers=[
        "Development Status :: 1 - Planning",
        "Topic :: Scientific/Engineering",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or "
        "later (GPLv3+)",
    ],
    entry_points={
        'console_scripts': [
            'dependu=dependu.cli:main',
        ],
    },

)
