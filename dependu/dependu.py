import pickle
import os
import unicodedata
import platform

class Result:
    def __init__(self,wordlist,cert,letters):
        self.wordlist = wordlist
        self.cert = cert
        self.letters = letters
    def show(self):
        print(str(self))
    def __str__(self):
        ret = ""
        if self.wordlist:
            l = len(self.wordlist)
            if l < 50:
                ret += "Suggestions de mots:\n"
                for w in self.wordlist:
                    ret += f"\t{w}\n"
            else:
                ret += f"{l} possibilités de mots\n"
        else:
            ret += "Aucune suggestion de mot\n"
        if self.letters:
            pourc = int(self.cert * 100)
            ret += f"\nSuggestions de lettres:\nCertitude: {pourc}%\n"
            for l in self.letters:
                ret += f"\t{l}\n"
        else:
            ret += "\nAucune suggestion de lettre\n"
        return ret

def get_conf_path():
    os_name = platform.system()
    if os_name == "Linux":
        path = os.path.expanduser("~/.config/dependu")
    elif os_name == "Windows":
        path = os.path.join(os.getenv("APPDATA"),"dependu")
    os.makedirs(path, exist_ok=True)
    return path

def strip_accents(s):
   return ''.join([c for c in unicodedata.normalize('NFD', s)
                  if unicodedata.category(c) != 'Mn'])
def get_valid_str(chaine):
    return strip_accents(chaine.strip().lower())

def get_dictionnary_path(conf_path):
    try:
        f = open(os.path.join(conf_path,"dict_path"),"r")
    except:
        dpath = input("Entrez le chemin du dictionnaire à utiliser\n>>>")
        f = open(os.path.join(conf_path,"dict_path"),"w")
        f.write(dpath)
        f.close()
        f = open(os.path.join(conf_path,"dict_path"),"r")
        return get_dictionnary_path(conf_path)
    return f.read()

def dict_reader(path):
    try:
        f = open(path,"r")
    except:
        raise Exception(f"Fichier introuvable: {path}")
    return f.readlines

def add_dict_entry(dictionnary,line):
	l = len(line)
	if "," in line:
		for w in line.split(","):
			add_dict_entry(dictionnary,w.strip())
	elif l not in dictionnary:
		dictionnary[l] = [line]
	elif line not in dictionnary[l]:
		dictionnary[l].append(line)

def create_dicts(dictlines, savepath):
    os.makedirs(savepath, exist_ok=True)
    dic = {}
    for n in map(get_valid_str,dictlines()):
    	add_dict_entry(dic,n)
    f = open(os.path.join(savepath,"dictionnary"), "wb")
    pickle.dump(dic, f)
    f.close()

def load_dicts(path):
    f = open(os.path.join(path,"dictionnary"), "rb")
    dic = pickle.load(f)
    f.close()
    return dic

def is_valid(word,guess,absent):
    told = guess + absent
    for l1, l2 in zip(word,guess):
        if (l2 == "." and l1 not in told) or l1 == l2:
            continue
        else:
            break
    else:
        return True
    return False

def load(conf_path):
    try:
        dic = load_dicts(conf_path)
    except:
        f = open(get_dictionnary_path(conf_path),"r")
        print("Création du dictionnaire...")
        create_dicts(f.readlines, conf_path)
        f.close()
        dic = load_dicts(conf_path)
    return dic

def get_possible_word_list(guess,absent_letters,dictionnary):
    conf_path = get_conf_path()
    l = len(guess) 
    if l not in dictionnary:
        return []
    return [word for word in dictionnary[l] if is_valid(word,guess,absent_letters)]

def get_result(guess,absent_letters,dictionnary):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    word_list = get_possible_word_list(guess,absent_letters,dictionnary)
    letters_occurences = {}
    remaining_letters = "".join([l for l in alphabet if l not in guess and l not in absent_letters])
    for word in word_list:
        for letter in remaining_letters:
            if letter in word:
                if letter not in letters_occurences:
                    letters_occurences[letter] = 1
                else:
                    letters_occurences[letter] += 1
    often_letters = []
    high_value = 0
    for letter in remaining_letters:
        try:
            n = letters_occurences[letter]
            if n == high_value :
                often_letters.append(letter)
            elif n > high_value:
                often_letters = [letter]
                high_value = n
            else:
                pass
        except:
            pass
    try:
        cert = high_value / len(word_list)
    except:
        cert = 0
    return Result(word_list, cert, often_letters)

def main():
    conf_path = get_conf_path()
    dictionnary = load(conf_path)
    guess = strip_accents(input("Entrez le mot\n>>>")).lower()
    absent_letters = strip_accents(input("Entrez les lettres absentes du mot\n>>>")).lower()
    res = get_result(guess,absent_letters,dictionnary)
    res.show()
if __name__ == "__main__":
    while True:
        try:
            main()
        except Exception as e:
            print(f"Error: {e}")
            
